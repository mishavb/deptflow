# DeptFlow

[![CI Status](http://img.shields.io/travis/Misha van Broekhoven/DeptFlow.svg?style=flat)](https://travis-ci.org/Misha van Broekhoven/DeptFlow)
[![Version](https://img.shields.io/cocoapods/v/DeptFlow.svg?style=flat)](http://cocoapods.org/pods/DeptFlow)
[![License](https://img.shields.io/cocoapods/l/DeptFlow.svg?style=flat)](http://cocoapods.org/pods/DeptFlow)
[![Platform](https://img.shields.io/cocoapods/p/DeptFlow.svg?style=flat)](http://cocoapods.org/pods/DeptFlow)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DeptFlow is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DeptFlow"
```

## Author

Misha van Broekhoven, misha@tamtam.nl

## License

DeptFlow is available under the MIT license. See the LICENSE file for more info.
