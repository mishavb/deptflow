//
//  FlowItem.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

public protocol FlowItemDelegate {
    func didChangeValueForItem(_ item: FlowItem)
}

public protocol FlowProperty {
    
}

#if swift(>=4.2)
public typealias UIViewContentMode = UIView.ContentMode
public typealias UITableViewRowAnimation = UITableView.RowAnimation
public typealias UIViewAnimationOptions = UIView.AnimationOptions
public let KeyboardWillShowNotification = UIResponder.keyboardWillShowNotification
public let KeyboardWillHideNotification = UIResponder.keyboardWillHideNotification
public let KeyboardFrameBeginUserInfoKey = UIResponder.keyboardFrameBeginUserInfoKey
@available(iOS 11.0, *)
public typealias UIScrollViewContentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior
#else
public let KeyboardWillShowNotification = NSNotification.Name.UIKeyboardWillShow
public let KeyboardWillHideNotification = NSNotification.Name.UIKeyboardWillHide
public let KeyboardFrameBeginUserInfoKey = UIKeyboardFrameBeginUserInfoKey
#endif


extension UIKeyboardType: FlowProperty {}
extension UITextAutocapitalizationType: FlowProperty {}
extension Date: FlowProperty {}
extension Bool: FlowProperty {}
extension String: FlowProperty {}
extension NSAttributedString: FlowProperty {}

extension CGRect :FlowProperty {}
extension CGPoint: FlowProperty {}
extension CGFloat: FlowProperty {}

extension UIImage: FlowProperty {}
extension UIColor: FlowProperty {}
extension UIEdgeInsets: FlowProperty {}

extension FlowItem: FlowProperty {}


open class FlowItem {
    
    public typealias FlowProperties = [AnyHashable: FlowProperty]
    public enum FlowItemError: Error {
        case fetchPropertyType
    }
    open var identifier: String = ""
    open var reuseIdentifier: String = ""
    open var bundle: Bundle = .main
    
    public var properties = FlowProperties()
    
    open var uniqueId: String = NSUUID().uuidString
    public let itemType: FlowItemType
    
    open var delegate:FlowItemDelegate?
    
    open var inputItem: FlowInputItem!
    open var staticItem: FlowStaticItem!
    open var navigationItem: FlowNavigationItem!
    open var flexItem: FlowFlexItem!
    
    open var height: CGFloat? = nil
    open var constant: CGFloat = 1
    
    public init(type: FlowItemType) {
        self.itemType = type
        
        switch type {
        case .input:
            self.inputItem = FlowInputItem(type: .default, delegate: self)
        case .static:
            self.staticItem = FlowStaticItem(type: .default)
        case .navigation:
            self.navigationItem = FlowNavigationItem(type: .default)
        case .flex:
            self.flexItem = FlowFlexItem()
        }
    }
    
    public init(inputType: FlowInputType) {
        self.itemType = .input
        self.inputItem = FlowInputItem(type: inputType, delegate: self)
    }
    
    public init(staticType: FlowStaticType) {
        self.itemType = .static
        self.staticItem = FlowStaticItem(type: staticType)
    }
    
    public init(navigationType: FlowNavigationType) {
        self.itemType = .navigation
        self.navigationItem = FlowNavigationItem(type: navigationType)
    }
    
    //MARK: FlowProperties
    
    open func addProperty<T: FlowProperty>(_ property: T, key: String) {
        properties[key] = property
    }
    
    open func fetchProperty<T: FlowProperty>(_ key: String) throws -> T {
        guard let property = properties[key] as? T else { throw FlowItemError.fetchPropertyType }
        return property
    }
    
    open func hasProperty(key: String) -> Bool {
        return properties[key] != nil ? true: false
    }
    
    //MARK: Register Cell
    
    open func registerCell<T: UITableViewCell>(type: T.Type, reuseIdentifier: String? = nil) {
        identifier = String(describing: type)
        self.reuseIdentifier = reuseIdentifier ?? identifier
        bundle = Bundle.init(for: type)
    }
    
    open func registerCell<T: UICollectionViewCell>(type: T.Type, reuseIdentifier: String? = nil) {
        identifier = String(describing: type)
        self.reuseIdentifier = reuseIdentifier ?? identifier
        bundle = Bundle.init(for: type)
    }
    
}

//MARK: - FlowInputItemDelegate
extension FlowItem: FlowInputItemDelegate {
    
    open func didChangeValue() {
        DispatchQueue.main.async(execute: {
            self.delegate?.didChangeValueForItem(self)
        })
        
    }
    
}

