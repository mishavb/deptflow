//
//  FlowDatasource.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

public protocol FlowDatasourceInputDelegate {
    func didUpdateValueForItem(_ item: FlowItem)
    func inputItemsAreValid(_ valid: Bool)
}

public extension FlowDatasourceInputDelegate {
    func didUpdateValueForItem(_ item: FlowItem) {}
    func inputItemsAreValid(_ valid: Bool) {}
    func addContactForItem(_ item: FlowItem) {}
}

public protocol FlowDatasourceDelegate {
    
    func didUpdateItemAtIndexPath(_ indexPath: IndexPath)
    func didInsertItemAtIndexPath(_ indexPath: IndexPath)
    func didRemoveItemAtIndexPath(_ indexPath: IndexPath)
    
    func didUpdateSectionsInIndexSet(_ indexSet: IndexSet)
    func didInsertSectionsInIndexSet(_ indexSet: IndexSet)
    func didRemoveSectionsInIndexSet(_ indexSet: IndexSet)
    
}

open class FlowDatasource: NSObject {
    
    open var inputDelegate: FlowDatasourceInputDelegate?
    open var delegate: FlowDatasourceDelegate?
    open var datasource: FlowDatasourceItems
    open var heights = [IndexPath: CGFloat]()
    
    override public init()
    {
        datasource = [FlowSection]()
    }
    
    open func getFirstInputResponder() -> FlowItem? {
        
        for section: FlowSection in datasource {
            
            for item in section {
                if let _ = item.inputItem {
                    return item
                }
            }
        }
        
        return nil
    }
    
}

public extension FlowDatasource {
    
    func getIndexPathBy(_ uniqueId: String) -> IndexPath? {
        for (section, sectionItem) in datasource.enumerated() {
            for (row, rowItem) in sectionItem.enumerated() {
                if rowItem.uniqueId == uniqueId {
                    return IndexPath(row: row, section: section)
                }
            }
        }
        return nil
    }
    
}

extension FlowDatasource: FlowItemDelegate {
    
    open func addContactForItem(_ item: FlowItem) {
        inputDelegate?.addContactForItem(item)
    }
    
    open func didChangeValueForItem(_ item: FlowItem) {
        inputDelegate?.didUpdateValueForItem(item)
        checkInputValues()
    }
    
    open func checkInputValues() {
        let items = datasource.flatMap { $0 }
        var valid = true
        for (_, item) in items.enumerated() {
            if valid == false {
                continue
            }
            
            if item.itemType == .input {
                if let validation = item.inputItem.validation {
                    valid = validation(item)
                }
            }
        }
        
        inputDelegate?.inputItemsAreValid(valid)
        
    }
    
}

public extension FlowDatasource {
    
    func getItem(_ indexPath: IndexPath) -> FlowItem?
    {
        return getItem(indexPath.section, inRow: indexPath.row)
    }
    
    func insertItem(_ indexPath: IndexPath, item: FlowItem)
    {
        insertItem(indexPath.section, inRow: indexPath.row, item: item)
    }
    
    func updateItem(_ indexPath: IndexPath, item: FlowItem)
    {
        updateItem(indexPath.section, inRow: indexPath.row, item: item)
    }
    
    func removeItem(_ indexPath: IndexPath)
    {
        removeItem(indexPath.section, inRow: indexPath.row)
    }
    
}

public extension FlowDatasource {
    
    func numberOfSections() -> Int
    {
        return datasource.count
    }
    
    func addSection(_ section: FlowSection)
    {
        for item in section {
            item.delegate = self
        }
        
        datasource.append(section)
        delegate?.didInsertSectionsInIndexSet(IndexSet(integer: datasource.count - 1))
    }
    
    func getSection(_ inSection: Int) -> FlowSection?
    {
        guard datasource.indices.contains(inSection) else { return nil }
        return datasource[inSection]
    }
    
    func insertSection(_ section: FlowSection, inSection: Int)
    {
        if inSection == datasource.count {
            addSection(section)
            return
        }
        
        guard datasource.indices.contains(inSection) else { return }
        datasource.insert(section, at: inSection)
        delegate?.didInsertSectionsInIndexSet(IndexSet(integer: inSection))
    }
    
    func updateSection(_ section: FlowSection, inSection: Int)
    {
        guard datasource.indices.contains(inSection) else { return insertSection(section, inSection: inSection) }
        datasource[inSection] = section
        delegate?.didUpdateSectionsInIndexSet(IndexSet(integer: inSection))
    }
    
    func removeSection(_ inSection: Int) {
        guard datasource.indices.contains(inSection) else { return }
        datasource.remove(at: inSection)
        delegate?.didRemoveSectionsInIndexSet(IndexSet(integer: inSection))
    }
    
    func removeSections(_ range: Range<Int>) {
        datasource.removeSubrange(range)
        delegate?.didRemoveSectionsInIndexSet(IndexSet(integersIn:range))
    }
    
}

public extension FlowDatasource
{
    func numberOfItemsInSection(_ inSection: Int) -> Int
    {
        guard datasource.indices.contains(inSection) else { return 0 }
        return datasource[inSection].count
    }
    
    func addItem(_ item: FlowItem, inSection: Int)
    {
        guard datasource.indices.contains(inSection) else { return }
        item.delegate = self
        datasource[inSection].append(item)
        
        delegate?.didInsertItemAtIndexPath(IndexPath(row: numberOfItemsInSection(inSection) - 1, section: inSection))
    }
    
    func getItem(_ inSection: Int, inRow: Int) -> FlowItem?
    {
        guard datasource.indices.contains(inSection) else { return nil }
        guard datasource[inSection].indices.contains(inRow) else { return nil }
        return datasource[inSection][inRow]
    }
    
    func insertItem(_ inSection: Int, inRow: Int, item: FlowItem)
    {
        guard datasource.indices.contains(inSection) else { return }
        
        if inRow == datasource[inSection].count {
            item.delegate = self
            addItem(item, inSection: inSection)
            return
        }
        
        guard datasource[inSection].indices.contains(inRow) else { return }
        
        datasource[inSection].insert(item, at: inRow)
        
        delegate?.didInsertItemAtIndexPath(IndexPath(row: inRow, section: inSection))
    }
    
    func updateItem(_ inSection: Int, inRow: Int, item: FlowItem)
    {
        guard datasource.indices.contains(inSection) else { return }
        guard datasource[inSection].indices.contains(inRow) else { return }
        item.delegate = self
        datasource[inSection][inRow] = item
        
        delegate?.didUpdateItemAtIndexPath(IndexPath(row: inRow, section: inSection))
    }
    
    func removeItem(_ inSection: Int, inRow: Int)
    {
        guard datasource.indices.contains(inSection) else { return }
        guard datasource[inSection].indices.contains(inRow) else { return }
        datasource[inSection].remove(at: inRow)
        
        delegate?.didRemoveItemAtIndexPath(IndexPath(row: inRow, section: inSection))
    }
}

