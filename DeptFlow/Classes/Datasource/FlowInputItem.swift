//
//  FlowInputItem.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

public protocol FlowInputItemDelegate {
    func didChangeValue()
}

public struct FlowInputItem {

    public let delegate: FlowInputItemDelegate?
    public let type: FlowInputType
    public var value: Any? {
        didSet {
            delegate?.didChangeValue()
            self.valueOnChange?(self.value)
        }
    }
    public var onSelectRow: FlowOnSelectRow?
    public var action: FlowNavigationAction?
    public var valueOnChange: FlowValueOnChange?
    public var validation: FlowValidation?

    init(type: FlowInputType, delegate: FlowInputItemDelegate) {
        self.type = type
        self.delegate = delegate
    }
}
