//
//  FlowNavigationItem.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import Foundation

public struct FlowNavigationItem {
    
    public init(type: FlowNavigationType) {
        self.type = type
    }
    public let type: FlowNavigationType
    public var action: FlowNavigationAction?
}
