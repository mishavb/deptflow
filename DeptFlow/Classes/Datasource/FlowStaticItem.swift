//
//  FlowStaticItem.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

public struct FlowStaticItem {
    
    public init(type: FlowStaticType) {
        self.type = type
    }
    public let type: FlowStaticType
    
}
