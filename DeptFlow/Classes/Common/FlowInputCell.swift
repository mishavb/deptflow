//
//  FlowInputCell.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import Foundation

public protocol FlowInputCell {
    func changeValue(_ value: Any?)
}

extension FlowInputCell {
    func changeValue(_ value: Any?) {}
}
