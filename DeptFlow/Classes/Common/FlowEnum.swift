//
//  FlowEnum.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import Foundation

public enum FlowItemType: Int
{
    case input
    case `static`
    case navigation
    case flex
}

public enum FlowInputType: Int
{
    case `default`
}

public enum FlowStaticType: Int
{
    case `default`
}

public enum FlowNavigationType: Int
{
    case `default`
}
