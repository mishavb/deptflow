//
//  FlowTypealiases.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import Foundation
import UIKit

public typealias FlowSection = [FlowItem]
public typealias FlowDatasourceItems = [FlowSection]
public typealias FlowValidation = (_ item: FlowItem) -> Bool
public typealias FlowValueOnChange = (_ value: Any?) -> ()
public typealias FlowNavigationAction = () -> Void
public typealias FlowOnSelectRow = (_ current: IndexPath, _ datasource: FlowDatasource) -> Void

