//
//  FlowValidation.swift
//  DeAlliantie Housing App
//
//  Created by Misha van Broekhoven on 02/06/16.
//  Copyright © 2016 Infinum. All rights reserved.
//

import Foundation

extension String {
    
    public var flowValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public var flowValidPostalCode: Bool {
        let postalCodeRegEx = "\\d{4}[ ]?[A-Za-z]{2}"
        let postalCodeTest = NSPredicate(format:"SELF MATCHES %@", postalCodeRegEx)
        return postalCodeTest.evaluate(with: self)
    }
    
    public var flowValidHouseNumber: Bool {
        let houseNumberCodeRegEx = "^[0-9]*$"
        let houseNumberTest = NSPredicate(format:"SELF MATCHES %@", houseNumberCodeRegEx)
        return houseNumberTest.evaluate(with: self)
    }
    
}
