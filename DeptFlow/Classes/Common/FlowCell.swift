//
//  FlowCell.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import Foundation

public protocol FlowCell {
    func setupForFlowItem(_ item: FlowItem, load: Bool)
}

extension FlowCell {
    func setupForFlowItem(_ item: FlowItem, load: Bool = false) {}
}
