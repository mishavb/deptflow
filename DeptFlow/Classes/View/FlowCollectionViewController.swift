//
//  FlowCollectionViewController.swift
//  Unive Verzekeringen
//
//  Created by Misha van Broekhoven on 13/06/16.
//  Copyright © 2016 TamTam. All rights reserved.
//

import UIKit

open class FlowCollectionViewController: UIViewController {
    
    @IBOutlet open var collectionView: UICollectionView!
    @IBOutlet open var backItem: UIBarButtonItem!
    @IBOutlet open var nextItem: UIBarButtonItem!
    open var datasource = FlowDatasource()
    open var identifiers = [String]()
    open var didUpdate: ((Bool) -> Void)?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        datasource.delegate = self
        automaticallyAdjustsScrollViewInsets = false
        edgesForExtendedLayout = UIRectEdge()
        collectionView.backgroundColor = .clear
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FlowCollectionViewController.keyboardWillShow(_:)), name: KeyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FlowCollectionViewController.keyboardWillHide(_:)), name: KeyboardWillHideNotification, object: nil)
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: KeyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: KeyboardWillHideNotification, object: nil)
    }
    
    open func registerCellForIdentifier(_ identifier: String, reuseIdentifier: String, bundle: Bundle = .main) {
        if identifiers.contains(identifier) {
            return
        }
        collectionView.register(UINib(nibName:identifier, bundle: bundle), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    open func cellForItem(_ item: FlowItem, indexPath: IndexPath? = nil) -> FlowCollectionBaseCell {
        
        var cell: FlowCell
        let identifier: String = item.identifier
        let reuseIdentifier: String = item.reuseIdentifier
        let bundle: Bundle = item.bundle
        
        
        registerCellForIdentifier(identifier, reuseIdentifier: reuseIdentifier, bundle: bundle)
        cell = cellForIdentifier(identifier, indexPath: indexPath)
        cell.setupForFlowItem(item, load: indexPath == nil ? true: false)
        
        return cell as! FlowCollectionBaseCell
    }
    
    open func cellForIdentifier<T>(_ identifier: String, indexPath: IndexPath?) -> T {
        
        var cell: T
        
        if let index = indexPath {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: index) as! T
        } else {
            cell = Bundle.main.loadNibNamed(identifier, owner: nil, options: nil)!.first as! T
        }
        
        return cell
    }
    
}

extension FlowCollectionViewController: UICollectionViewDelegate {
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = datasource.getItem(indexPath)!
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if item.itemType == .navigation {
            item.navigationItem.action?()
        }
        
        if item.itemType == .input {
            item.inputItem.onSelectRow?(indexPath, datasource)
        }
    }
}

extension FlowCollectionViewController: UICollectionViewDataSource {
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.numberOfItemsInSection(section)
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = datasource.getItem(indexPath)!
        let cell: FlowCollectionBaseCell = cellForItem(item, indexPath: indexPath)
        return cell
    }
    
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        return datasource.numberOfSections()
    }
    
}

extension FlowCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width / 2.0, height: collectionView.bounds.width / 2.0)
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
}

extension FlowCollectionViewController: FlowDatasourceDelegate {
    
    open func didUpdateItemAtIndexPath(_ indexPath: IndexPath) {
        collectionView.performBatchUpdates({
            self.collectionView.reloadItems(at: [indexPath])
        }, completion: didUpdate)
        
    }
    
    open func didInsertItemAtIndexPath(_ indexPath: IndexPath) {
        collectionView.performBatchUpdates({
            self.collectionView.insertItems(at: [indexPath])
        }, completion: didUpdate)
        
    }
    
    open func didRemoveItemAtIndexPath(_ indexPath: IndexPath) {
        collectionView.performBatchUpdates({
            self.collectionView.deleteItems(at: [indexPath])
        }, completion: didUpdate)
        
    }
    
    open func didUpdateSectionsInIndexSet(_ indexSet: IndexSet) {
        collectionView.performBatchUpdates({
            self.collectionView.reloadSections(indexSet)
        }, completion: didUpdate)
        
    }
    
    open func didInsertSectionsInIndexSet(_ indexSet: IndexSet) {
        collectionView.performBatchUpdates({
            self.collectionView.insertSections(indexSet)
        }, completion: didUpdate)
        
    }
    
    open func didRemoveSectionsInIndexSet(_ indexSet: IndexSet) {
        collectionView.performBatchUpdates({
            self.collectionView.deleteSections(indexSet)
        }, completion: didUpdate)
        
    }
    
}

public extension FlowCollectionViewController {
    
    @objc func keyboardWillShow(_ notification:Notification) {
        if let keyboardSize = (notification.userInfo?[KeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                self.collectionView.contentInset.bottom = keyboardSize.height
            }, completion: nil)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            self.collectionView.contentInset.bottom = 0
        }, completion: nil)
    }
}

