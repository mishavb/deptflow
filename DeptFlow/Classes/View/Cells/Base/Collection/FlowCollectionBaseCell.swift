//
//  FlowCollectionBaseCell.swift
//  Unive Verzekeringen
//
//  Created by Misha van Broekhoven on 13/06/16.
//  Copyright © 2016 TamTam. All rights reserved.
//

import UIKit

open class FlowCollectionBaseCell: UICollectionViewCell {
    
    open var item: FlowItem?
    
}
