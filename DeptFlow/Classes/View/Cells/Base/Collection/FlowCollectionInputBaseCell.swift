//
//  FlowCollectionInputBaseCell.swift
//  Unive Verzekeringen
//
//  Created by Misha van Broekhoven on 13/06/16.
//  Copyright © 2016 TamTam. All rights reserved.
//

import UIKit

open class FlowCollectionInputBaseCell: FlowCollectionBaseCell {

    open var inputItem: FlowInputItem {
        get {
            return item!.inputItem
        }
        set {
            return item!.inputItem = newValue
        }
    }

}
