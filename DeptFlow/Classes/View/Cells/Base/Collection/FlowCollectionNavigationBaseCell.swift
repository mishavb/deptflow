//
//  FlowCollectionNavigationBaseCell.swift
//  Unive Verzekeringen
//
//  Created by Misha van Broekhoven on 13/06/16.
//  Copyright © 2016 TamTam. All rights reserved.
//

import UIKit

open class FlowCollectionNavigationBaseCell: FlowCollectionBaseCell {

    open var pressed = false
    open var navigationItem: FlowNavigationItem {
        get {
            return item!.navigationItem
        }
    }

}
