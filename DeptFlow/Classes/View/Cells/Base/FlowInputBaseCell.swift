//
//  FlowInputBaseCell.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

open class FlowInputBaseCell: FlowBaseCell {

    open var pressed = false
    open var inputItem: FlowInputItem {
        get {
            return item!.inputItem
        }
        set {
            return item!.inputItem = newValue
        }
    }
}
