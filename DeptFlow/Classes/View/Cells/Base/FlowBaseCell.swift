//
//  FlowBaseCell.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

open class FlowBaseCell: UITableViewCell {
    
    open var item: FlowItem?
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        separatorInset = UIEdgeInsets.zero
        preservesSuperviewLayoutMargins = false
        layoutMargins = UIEdgeInsets.zero
    }
}
