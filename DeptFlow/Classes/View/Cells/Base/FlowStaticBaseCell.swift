//
//  FlowStaticBaseCell.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

open class FlowStaticBaseCell: FlowBaseCell {

    open var staticItem: FlowStaticItem {
        get {
            return item!.staticItem
        }
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }

}
