//
//  FlowNavigationBaseCell.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

open class FlowNavigationBaseCell: FlowBaseCell {
    
    open var pressed = false
    open var navigationItem: FlowNavigationItem {
        get {
            return item!.navigationItem
        }
    }

}
