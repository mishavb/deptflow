//
//  FlowFlexBaseCell.swift
//  DeptFlow
//
//  Created by Misha van Broekhoven on 19/10/2017.
//

import Foundation

open class FlowFlexBaseCell: FlowBaseCell {
    
    open var flexItem: FlowFlexItem {
        get {
            return item!.flexItem
        }
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
}
