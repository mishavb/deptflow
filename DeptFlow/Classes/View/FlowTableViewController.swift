
//
//  FlowTableViewController.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import UIKit

public protocol FlowView: class {
    var datasource: FlowDatasource { get set }
    var backItem: UIBarButtonItem! { get set }
    var nextItem: UIBarButtonItem! { get set }
}

open class FlowTableViewController: UIViewController
{
    @IBOutlet open var tableView: UITableView!
    @IBOutlet open var backItem: UIBarButtonItem!
    @IBOutlet open var nextItem: UIBarButtonItem!
    open var datasource = FlowDatasource()
    open var identifiers = [String]()
    open var animation: UITableViewRowAnimation = .none
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        datasource.delegate = self
        automaticallyAdjustsScrollViewInsets = false
        edgesForExtendedLayout = UIRectEdge()
        
        let header = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.001))
        header.backgroundColor = .clear
        tableView.tableHeaderView = header
        tableView.tableFooterView = nil
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.backgroundColor = .clear
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            // Fallback on earlier versions
        }
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FlowTableViewController.keyboardWillShow(_:)), name: KeyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FlowTableViewController.keyboardWillHide(_:)), name: KeyboardWillHideNotification, object: nil)
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: KeyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: KeyboardWillHideNotification, object: nil)
    }
    
    open func registerCellForIdentifier(_ identifier: String, reuseIdentifier: String, bundle: Bundle = .main) {
        if identifiers.contains(reuseIdentifier) {
            return
        }
        identifiers.append(reuseIdentifier)
        tableView.register(UINib(nibName:identifier, bundle: bundle) , forCellReuseIdentifier: reuseIdentifier)
    }
    
    open func cellForItem(_ item: FlowItem, indexPath: IndexPath? = nil) -> FlowBaseCell {
        
        var cell: FlowCell
        let identifier: String = item.identifier
        let reuseIdentifier: String = item.reuseIdentifier
        let bundle: Bundle = item.bundle
        
        
        registerCellForIdentifier(identifier, reuseIdentifier: reuseIdentifier, bundle: bundle)
        cell = cellForIdentifier(reuseIdentifier, indexPath: indexPath)
        cell.setupForFlowItem(item, load: indexPath == nil ? true: false)
        
        return cell as! FlowBaseCell
    }
    
    open func cellForIdentifier<T>(_ identifier: String, indexPath: IndexPath?) -> T {
        
        var cell: T
        
        if let index = indexPath {
            cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: index) as! T
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! T
        }
        
        return cell
    }
    
}

extension FlowTableViewController: UITableViewDelegate {
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = datasource.getItem(indexPath)!
        
        if item.itemType == .navigation {
            item.navigationItem.action?()
        }
        
        if item.itemType == .input {
            item.inputItem.onSelectRow?(indexPath, datasource)
        }
    }
}

extension FlowTableViewController: UITableViewDataSource {
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.numberOfSections()
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.numberOfItemsInSection(section)
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    open func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = datasource.getItem(indexPath) else { return FlowBaseCell() }
        let cell: FlowBaseCell = cellForItem(item)
        return cell
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let height = datasource.heights[indexPath] {
            return height
        }
        
        let item = datasource.getItem(indexPath)!
        
        if let height = item.height {
            return height
        }
        
        //flex item logic
        if item.flexItem != nil {
            let constant = item.constant
            let height: CGFloat = tableView.bounds.height - heightForFlexibleCells()
            if height > 0 {
                return height * constant
            } else {
                return 0
            }
        }
        let cell: FlowBaseCell = cellForItem(item)
        let size = CGSize(width: tableView.bounds.width, height: 0)
        let height: CGFloat = cell.contentView.systemLayoutSizeFitting(size).height + 0.5
        datasource.heights[indexPath] = height
        return height
        
    }
}

extension FlowTableViewController: FlowDatasourceDelegate {
    
    open func didUpdateItemAtIndexPath(_ indexPath: IndexPath) {
        enableAnimationsIfNeeded(animation: animation)
        tableView.reloadRows(at: [indexPath], with: animation)
        enableAnimationsIfNeeded()
    }
    
    open func didInsertItemAtIndexPath(_ indexPath: IndexPath) {
        enableAnimationsIfNeeded(animation: animation)
        tableView.insertRows(at: [indexPath], with: animation)
        enableAnimationsIfNeeded()
    }
    
    open func didRemoveItemAtIndexPath(_ indexPath: IndexPath) {
        enableAnimationsIfNeeded(animation: animation)
        tableView.deleteRows(at: [indexPath], with: animation)
        enableAnimationsIfNeeded()
    }
    
    open func didUpdateSectionsInIndexSet(_ indexSet: IndexSet) {
        enableAnimationsIfNeeded(animation: animation)
        tableView.reloadSections(indexSet, with: animation)
        enableAnimationsIfNeeded()
    }
    
    open func didInsertSectionsInIndexSet(_ indexSet: IndexSet) {
        enableAnimationsIfNeeded(animation: animation)
        tableView.insertSections(indexSet, with: animation)
        enableAnimationsIfNeeded()
    }
    
    open func didRemoveSectionsInIndexSet(_ indexSet: IndexSet) {
        enableAnimationsIfNeeded(animation: animation)
        tableView.deleteSections(indexSet, with: .none)
        enableAnimationsIfNeeded()
    }
    
    private func enableAnimationsIfNeeded(animation: UITableViewRowAnimation? = nil) {
        let isUITestRunning = ProcessInfo.processInfo.arguments.contains("Mock")
        if let animation = animation {
            UIView.setAnimationsEnabled(animation != .none)
        } else {
            UIView.setAnimationsEnabled(isUITestRunning ? false: true)
        }
    }
    
}

extension FlowTableViewController {
    
    @objc open func keyboardWillShow(_ notification:Notification) {
        if let keyboardSize = (notification.userInfo?[KeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                self.tableView.contentInset.bottom = keyboardSize.height
            }, completion: nil)
        }
    }
    
    @objc open func keyboardWillHide(_ notification:Notification) {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            self.tableView.contentInset.bottom = 0
        }, completion: nil)
    }
}

extension FlowTableViewController {
    
    open func heightForFlexibleCells() -> CGFloat {
        var cells = [FlowBaseCell]()
        var indexPaths = [IndexPath]()
        var heights = [CGFloat]()
        let items = datasource.datasource.flatMap({ $0 })
        
        let numberOfSections = 0..<datasource.datasource.count
        for section in numberOfSections {
            let rows = 0..<datasource.datasource[section].count
            for row in rows {
                let indexPath = IndexPath(row: row, section: section)
                indexPaths.append(indexPath)
            }
            let footer = tableView.delegate?.tableView?(tableView!, heightForFooterInSection: section) ?? 0
            let header = tableView.delegate?.tableView?(tableView!, heightForHeaderInSection: section) ?? 0
            heights.append(footer)
            heights.append(header)
        }
        
        
        if #available(iOS 11.0, *) {
            heights.append(tableView.adjustedContentInset.top)
            heights.append(tableView.adjustedContentInset.bottom)
        } else {
            heights.append(tableView.contentInset.top)
            heights.append(tableView.contentInset.bottom)
        }
        
        for (index, item) in items.enumerated() {
            
            
            if item.flexItem != nil {
                continue
            }
            let indexPath = indexPaths[index]
            let identifier: String = item.identifier
            let reuseIdentifier: String = item.reuseIdentifier
            let bundle: Bundle = item.bundle
            
            
            registerCellForIdentifier(identifier, reuseIdentifier: reuseIdentifier, bundle: bundle)
            let cell: FlowBaseCell = cellForIdentifier(item.reuseIdentifier, indexPath: nil)
            
            if let cell = cell as? FlowCell {
                cell.setupForFlowItem(item, load: false)
            }
            
            cells.append(cell)
            
            if let height = datasource.heights[indexPath] {
                heights.append(height)
                continue
            }
            
            let item = datasource.getItem(indexPath)!
            
            if let height = item.height {
                heights.append(height)
                continue
            }
            
            let height: CGFloat = cell.contentView.systemLayoutSizeFitting(CGSize(width: tableView.bounds.width, height: 0)).height + 0.5
            datasource.heights[indexPath] = height
            heights.append(height)
        }
        return heights.reduce(0, { $0 + $1 })
    }
    
}

