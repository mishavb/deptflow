//
//  FlowPresenter.swift
//  AlliantieFlow
//
//  Created by Misha van Broekhoven on 24/05/16.
//  Copyright © 2016 Misha van Broekhoven. All rights reserved.
//

import Foundation

@objc public protocol FlowPresenterDelegate: class {
    func initializeDatasource()
}

open class FlowPresenter: NSObject {
    
    open func didUpdateValueForItem(_ item: FlowItem) { }
    open func inputItemsAreValid(_ valid: Bool) { }
    
}

extension FlowPresenter: FlowDatasourceInputDelegate {
    
}
